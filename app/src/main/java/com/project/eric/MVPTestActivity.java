package com.project.eric;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.project.eric.mvp.bean.CityBean;
import com.project.eric.mvp.presenter.MyPresenter;
import com.project.eric.mvp.view.IView;


public class MVPTestActivity extends AppCompatActivity implements IView<CityBean>{

    private TextView text_mvp;
    private MyPresenter myPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mvptest);
        text_mvp = (TextView) findViewById(R.id.text_mvp);
        myPresenter = new MyPresenter();
        myPresenter.attachView(this);
        myPresenter.fetch();
    }


    @Override
    public void showLoading() {
        text_mvp.setText("数据获取中...");
    }

    @Override
    public void showView(final CityBean data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text_mvp.setText("城市："  + data.getCity() + "  温度：" + data.getData().getWendu() + "度。");
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        myPresenter.detachView();
    }


}
