package com.project.eric;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.project.eric.fragment.BaseFragment;
import com.project.eric.fragment.NoParamNoResultFragment;
import com.project.eric.strct.FunctionManager;
import com.project.eric.strct.FunctionNoParamNoResult;
import com.project.eric.strct.FunctionWithParamOnly;
import com.project.eric.strct.FunctionWithParamWithResult;
import com.project.eric.strct.FunctionWithResultOnly;

public class MainActivity extends AppCompatActivity {

    public static final String FunctionNoParamNoResult = "FunctionNoParamNoResult";
    public static final String FunctionWithResultOnly = "FunctionWithResultOnly";
    public static final String FunctionWithParamOnly = "FunctionWithParamOnly";
    public static final String FunctionWithParamWithResult = "FunctionWithParamWithResult";

    private FragmentManager fm;

    private Handler  handler =  new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0x123:
                    String message = (String) msg.obj;
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        NoParamNoResultFragment noParamNoResultFragment = new NoParamNoResultFragment(handler);
        ft.add(R.id.fragment,noParamNoResultFragment,NoParamNoResultFragment.class.getName());
        ft.commit();
    }


    public void setFunctionForFragment(String tag){
        if (TextUtils.isEmpty(tag)){
            Log.e(MainActivity.class.getSimpleName(),"tag is null !");
            return;
        }
        BaseFragment fragment = (BaseFragment) fm.findFragmentByTag(tag);
        FunctionManager functionManager = FunctionManager.getInstance();

        functionManager.addFunction(new FunctionNoParamNoResult(FunctionNoParamNoResult) {
            @Override
            public void function() {
                Toast.makeText(MainActivity.this, "无参无返回值", Toast.LENGTH_SHORT).show();
            }
        });

        functionManager.addFunction(new FunctionWithResultOnly<String>(FunctionWithResultOnly) {
            @Override
            public String function() {
                return "无参有返回值";
            }
        });

        functionManager.addFunction(new FunctionWithParamOnly<String>(FunctionWithParamOnly) {
            @Override
            public void function(String o) {
                Toast.makeText(MainActivity.this, o, Toast.LENGTH_SHORT).show();
            }
        });

        functionManager.addFunction(new FunctionWithParamWithResult<String,String>(FunctionWithParamWithResult) {
            @Override
            public String function(String o) {
                return o;
            }
        });

        fragment.setFunctionManager(functionManager);
    }




}
