package com.project.eric.strct;

/**
 *  有参无返回值
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/4 16:13
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */

public abstract class FunctionWithParamOnly<Param> extends Function {

    public FunctionWithParamOnly(String funName) {
        super(funName);
    }

    public abstract void function(Param param);


}
