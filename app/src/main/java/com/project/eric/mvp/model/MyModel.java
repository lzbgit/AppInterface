package com.project.eric.mvp.model;

import com.alibaba.fastjson.JSON;
import com.project.eric.Http.HttpUtils;
import com.project.eric.Http.RequestCallBack;
import com.project.eric.mvp.bean.CityBean;

/**
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/18 14:17
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public class MyModel implements IModel{

    String weatherPath = "http://www.sojson.com/open/api/weather/json.shtml?city=北京";
    private HttpUtils httpUtils;

    @Override
    public void loaderData(final OnLoaderDataListener onLoaderDataListener) {
        httpUtils = HttpUtils.getInstance();
        new Thread(new Runnable() {
            @Override
            public void run() {
                httpUtils.get(weatherPath, new RequestCallBack() {
                    @Override
                    public void onCall(Object o, Exception e) {
                        if (null != o){
                            String result  = (String) o;
                            CityBean cityBean  = JSON.parseObject(result,CityBean.class);
                            onLoaderDataListener.onLoader(cityBean);
                        }
                    }
                });
            }
        }).start();

    }


}
