package com.project.eric.mvp.model;

/**
 * mvp model层
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/18 11:08
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public interface IModel {
    /**
     * 加载数据
     */
    void loaderData(OnLoaderDataListener onLoaderDataListener);


    /**
     * 对数据进行监听 获取数据后，更新视图
     *<h3>Description</h3>
     * TODO
     *<h3>Author</h3> luzhenbang
     *<h3>Date</h3> 2018/1/18 14:14
     *<h3>Copyright</h3> Copyright (c)${YEAR} Shenzhen TL  Co., Ltd. Inc. All rights reserved.
     */
    interface OnLoaderDataListener<T> {
        void onLoader(T data);
    }





}
