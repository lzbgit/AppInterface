package com.project.eric.mvp.presenter;

import android.util.Log;

import com.project.eric.mvp.model.IModel;
import com.project.eric.mvp.view.IView;

import java.lang.ref.WeakReference;

/**
 * MVP 表示层基类
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/18 11:16
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */

public abstract class BasePresenter<V,T extends IView<V>> {

    public static final String TAG  = BasePresenter.class.getSimpleName();

    private WeakReference<T> mIView = null;

    private IModel mModel = null;

    public BasePresenter() {

    }


    /**
     * 绑定View 即VIew层与表示层建立了连接
     * @param view
     */
    public void attachView(T view){
        this.mIView = new WeakReference<T>(view);
        this.mModel = createIModel();
    }


    /**
     * 解绑VIew
     */
    public void detachView(){
        if (null != mIView){
            mIView.clear();
        }
    }

    /**
     * 创建Model
     * @return
     */
    public abstract IModel createIModel();

    /**
     * 操作
     */
    public void fetch (){
        if (null != mIView.get()){
            mIView.get().showLoading();
            if (null != mModel){
                mModel.loaderData(new IModel.OnLoaderDataListener<V>() {
                    @Override
                    public void onLoader(V data) {
                        mIView.get().showView(data);
                    }
                });
            }else {
                Log.e(TAG,"mModel is null ,please firstly invoke createIModel method ！");
            }
        }else {
            Log.e(TAG,"mIView is null ,please firstly invoke attachView method ！");
        }


    }



}
