package com.project.eric.mvp.bean;

import java.util.List;

/**
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/18 14:55
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public class CityBean {

    /**
     * date : 20180118
     * message : Success !
     * status : 200
     * city : 北京
     * count : 702
     * data : {"shidu":"23%","pm25":10,"pm10":28,"quality":"优","wendu":"-1","ganmao":"各类人群可自由活动","yesterday":{"date":"17日星期三","sunrise":"07:34","high":"高温 6.0℃","low":"低温 -7.0℃","sunset":"17:15","aqi":62,"fx":"西北风","fl":"<3级","type":"多云","notice":"阴晴之间，谨防紫外线侵扰"},"forecast":[{"date":"18日星期四","sunrise":"07:34","high":"高温 5.0℃","low":"低温 -6.0℃","sunset":"17:16","aqi":89,"fx":"西南风","fl":"<3级","type":"晴","notice":"愿你拥有比阳光明媚的心情"},{"date":"19日星期五","sunrise":"07:33","high":"高温 7.0℃","low":"低温 -5.0℃","sunset":"17:17","aqi":131,"fx":"西南风","fl":"<3级","type":"晴","notice":"愿你拥有比阳光明媚的心情"},{"date":"20日星期六","sunrise":"07:33","high":"高温 5.0℃","low":"低温 -5.0℃","sunset":"17:18","aqi":113,"fx":"东南风","fl":"<3级","type":"晴","notice":"愿你拥有比阳光明媚的心情"},{"date":"21日星期日","sunrise":"07:32","high":"高温 2.0℃","low":"低温 -4.0℃","sunset":"17:19","aqi":71,"fx":"东北风","fl":"<3级","type":"阴","notice":"不要被阴云遮挡住好心情"},{"date":"22日星期一","sunrise":"07:32","high":"高温 1.0℃","low":"低温 -6.0℃","sunset":"17:20","aqi":57,"fx":"北风","fl":"<3级","type":"阴","notice":"不要被阴云遮挡住好心情"}]}
     */

    private String date;
    private String message;
    private int status;
    private String city;
    private int count;
    /**
     * shidu : 23%
     * pm25 : 10
     * pm10 : 28
     * quality : 优
     * wendu : -1
     * ganmao : 各类人群可自由活动
     * yesterday : {"date":"17日星期三","sunrise":"07:34","high":"高温 6.0℃","low":"低温 -7.0℃","sunset":"17:15","aqi":62,"fx":"西北风","fl":"<3级","type":"多云","notice":"阴晴之间，谨防紫外线侵扰"}
     * forecast : [{"date":"18日星期四","sunrise":"07:34","high":"高温 5.0℃","low":"低温 -6.0℃","sunset":"17:16","aqi":89,"fx":"西南风","fl":"<3级","type":"晴","notice":"愿你拥有比阳光明媚的心情"},{"date":"19日星期五","sunrise":"07:33","high":"高温 7.0℃","low":"低温 -5.0℃","sunset":"17:17","aqi":131,"fx":"西南风","fl":"<3级","type":"晴","notice":"愿你拥有比阳光明媚的心情"},{"date":"20日星期六","sunrise":"07:33","high":"高温 5.0℃","low":"低温 -5.0℃","sunset":"17:18","aqi":113,"fx":"东南风","fl":"<3级","type":"晴","notice":"愿你拥有比阳光明媚的心情"},{"date":"21日星期日","sunrise":"07:32","high":"高温 2.0℃","low":"低温 -4.0℃","sunset":"17:19","aqi":71,"fx":"东北风","fl":"<3级","type":"阴","notice":"不要被阴云遮挡住好心情"},{"date":"22日星期一","sunrise":"07:32","high":"高温 1.0℃","low":"低温 -6.0℃","sunset":"17:20","aqi":57,"fx":"北风","fl":"<3级","type":"阴","notice":"不要被阴云遮挡住好心情"}]
     */

    private DataBean data;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private String shidu;
        private int pm25;
        private int pm10;
        private String quality;
        private String wendu;
        private String ganmao;
        /**
         * date : 17日星期三
         * sunrise : 07:34
         * high : 高温 6.0℃
         * low : 低温 -7.0℃
         * sunset : 17:15
         * aqi : 62
         * fx : 西北风
         * fl : <3级
         * type : 多云
         * notice : 阴晴之间，谨防紫外线侵扰
         */

        private YesterdayBean yesterday;
        /**
         * date : 18日星期四
         * sunrise : 07:34
         * high : 高温 5.0℃
         * low : 低温 -6.0℃
         * sunset : 17:16
         * aqi : 89
         * fx : 西南风
         * fl : <3级
         * type : 晴
         * notice : 愿你拥有比阳光明媚的心情
         */

        private List<ForecastBean> forecast;

        public String getShidu() {
            return shidu;
        }

        public void setShidu(String shidu) {
            this.shidu = shidu;
        }

        public int getPm25() {
            return pm25;
        }

        public void setPm25(int pm25) {
            this.pm25 = pm25;
        }

        public int getPm10() {
            return pm10;
        }

        public void setPm10(int pm10) {
            this.pm10 = pm10;
        }

        public String getQuality() {
            return quality;
        }

        public void setQuality(String quality) {
            this.quality = quality;
        }

        public String getWendu() {
            return wendu;
        }

        public void setWendu(String wendu) {
            this.wendu = wendu;
        }

        public String getGanmao() {
            return ganmao;
        }

        public void setGanmao(String ganmao) {
            this.ganmao = ganmao;
        }

        public YesterdayBean getYesterday() {
            return yesterday;
        }

        public void setYesterday(YesterdayBean yesterday) {
            this.yesterday = yesterday;
        }

        public List<ForecastBean> getForecast() {
            return forecast;
        }

        public void setForecast(List<ForecastBean> forecast) {
            this.forecast = forecast;
        }

        public static class YesterdayBean {
            private String date;
            private String sunrise;
            private String high;
            private String low;
            private String sunset;
            private int aqi;
            private String fx;
            private String fl;
            private String type;
            private String notice;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getSunrise() {
                return sunrise;
            }

            public void setSunrise(String sunrise) {
                this.sunrise = sunrise;
            }

            public String getHigh() {
                return high;
            }

            public void setHigh(String high) {
                this.high = high;
            }

            public String getLow() {
                return low;
            }

            public void setLow(String low) {
                this.low = low;
            }

            public String getSunset() {
                return sunset;
            }

            public void setSunset(String sunset) {
                this.sunset = sunset;
            }

            public int getAqi() {
                return aqi;
            }

            public void setAqi(int aqi) {
                this.aqi = aqi;
            }

            public String getFx() {
                return fx;
            }

            public void setFx(String fx) {
                this.fx = fx;
            }

            public String getFl() {
                return fl;
            }

            public void setFl(String fl) {
                this.fl = fl;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getNotice() {
                return notice;
            }

            public void setNotice(String notice) {
                this.notice = notice;
            }
        }

        public static class ForecastBean {
            private String date;
            private String sunrise;
            private String high;
            private String low;
            private String sunset;
            private int aqi;
            private String fx;
            private String fl;
            private String type;
            private String notice;

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getSunrise() {
                return sunrise;
            }

            public void setSunrise(String sunrise) {
                this.sunrise = sunrise;
            }

            public String getHigh() {
                return high;
            }

            public void setHigh(String high) {
                this.high = high;
            }

            public String getLow() {
                return low;
            }

            public void setLow(String low) {
                this.low = low;
            }

            public String getSunset() {
                return sunset;
            }

            public void setSunset(String sunset) {
                this.sunset = sunset;
            }

            public int getAqi() {
                return aqi;
            }

            public void setAqi(int aqi) {
                this.aqi = aqi;
            }

            public String getFx() {
                return fx;
            }

            public void setFx(String fx) {
                this.fx = fx;
            }

            public String getFl() {
                return fl;
            }

            public void setFl(String fl) {
                this.fl = fl;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getNotice() {
                return notice;
            }

            public void setNotice(String notice) {
                this.notice = notice;
            }
        }
    }
}
