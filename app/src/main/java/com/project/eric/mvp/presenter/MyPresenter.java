package com.project.eric.mvp.presenter;

import com.project.eric.mvp.bean.CityBean;
import com.project.eric.mvp.model.IModel;
import com.project.eric.mvp.model.MyModel;
import com.project.eric.mvp.view.IView;

/**
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/18 15:29
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public class MyPresenter extends BasePresenter<CityBean,IView<CityBean>>{

    @Override
    public IModel createIModel() {
        return new MyModel();
    }



}
