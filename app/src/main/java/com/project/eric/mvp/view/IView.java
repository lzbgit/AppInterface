package com.project.eric.mvp.view;

/**
 * mvp view层
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/18 10:48
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public interface IView<T> {

    void showLoading();
    /**
     * 显示视图
     */
    void showView(T data);


}
