package com.project.eric.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.project.eric.MainActivity;
import com.project.eric.strct.FunctionManager;

/**
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2018/1/4 17:12
 * <h3>Copyright</h3> Copyright (c)2018 Shenzhen TL  Co., Ltd. Inc. All rights reserved.
 */
public class BaseFragment extends Fragment{

    public FunctionManager mFunctionManager;
    private MainActivity mainActivity;

    public void setFunctionManager(FunctionManager mFunctionManager) {
        this.mFunctionManager = mFunctionManager;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            mainActivity  = (MainActivity) context;
            mainActivity.setFunctionForFragment(getTag());
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainActivity = null;
    }

}
