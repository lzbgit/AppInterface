package testapp.testokhttp.okhttp;

import java.io.Serializable;

/**
 * <h3>Description</h3> UI进度回调实体类
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2016/6/29 10:44
 * <h3>Copyright</h3> Copyright (c)2016 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public class ProgressModel implements Serializable {
    /** 当前读取字节长度*/
    private long currentBytes;
    /** 总字节长度*/
    private long contentLength;
    /** 是否读取完成*/
    private boolean done;

    public ProgressModel(long currentBytes, long contentLength, boolean done) {
        this.currentBytes = currentBytes;
        this.contentLength = contentLength;
        this.done = done;
    }

    public long getCurrentBytes() {
        return currentBytes;
    }

    public void setCurrentBytes(long currentBytes) {
        this.currentBytes = currentBytes;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return "ProgressModel{" +
                "currentBytes=" + currentBytes +
                ", contentLength=" + contentLength +
                ", done=" + done +
                '}';
    }
}
