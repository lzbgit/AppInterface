package testapp.testokhttp.okhttp;

/**
 * <h3>Description</h3> 响应体进度回调接口，比如用于文件下载中
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2016/6/29 10:18
 * <h3>Copyright</h3> Copyright (c)2016 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public interface ProgressResponseListener {
    /**
     *
     * @param bytesRead 每次读取的文件的长度
     * @param contentLength 文件长度的长度
     * @param done true 下载完成 否则未完成
     */
    void onResponseProgress(long bytesRead, long contentLength, boolean done);
}
