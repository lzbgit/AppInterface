package testapp.testokhttp.okhttp;

/**
 * <h3>Description</h3>请求体进度回调接口，比如用于文件上传中
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2016/6/29 10:29
 * <h3>Copyright</h3> Copyright (c)2016 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public interface ProgressRequestListener {
    void onRequestProgress(long bytesWritten, long contentLength, boolean done);
}