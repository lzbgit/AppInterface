package testapp.testokhttp.okhttp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.net.FileNameMap;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import testapp.testokhttp.R;

/**
 * <h3>Description</h3> OKHttp网络请求工具类  OKHttp网络请求有重要的四个类：
 * Request 请求对象 RequestBody 请求体（可以携带参数;上传文件）
 * Response 响应对象 ResponseBody 响应体（携带响应数据）
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2016/4/6 20:14
 * <h3>Copyright</h3> Copyright (c)2016 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public class OKHttpUtils {
    /**====================================需要根据后台返回数据配置的字段 start================================*/
    /** 状态码字段 */
    public static final String OKHTTP_STATUS = "rspcode";
    /** 数据字段*/
    public static final String OKHTTP_DATA = "data";
    /** 请求信息字段*/
    public static final String OKHTTP_INFO = "msg";
    /**====================================需要根据后台返回数据配置的字段 end================================*/

    /** OkHttp客户端*/
    private static  OkHttpClient okHttpClient;
    private Call call;
    /** 上传文件 */
    public static final MediaType MEDIA_TYPE_MARKDOWN = MediaType.parse("text/x-markdown; charset=utf-8");
    /**参数类型 */
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    /** 连接超时*/
    public static final int CONNECT_OUT_TIME = 5000;
    /** 读取超时*/
    public static final int READ_OUT_TIME = 5000;
    /** 写超时*/
    public static final int WRITE_OUT_TIME = 5000;
    private static OKHttpUtils httpUtils;
    private final Context context;
    /** 接口回调*/
    private RequestCallInterface requestCallInterf;

    /** 请求结果返回*/
    public static final int RESULT = 30;

    /** 请求得到的数据*/
    private Handler handler = new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (requestCallInterf != null && msg.what == RESULT && msg.obj != null){
                requestCallInterf.response((ResponseBean) msg.obj);
            }
        }
    };
    private Object signRecord;

    /**
     * 构造方法私有化
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,9:21
     * <h3>UpdateTime</h3> 2016/4/7,9:21
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     */
    private OKHttpUtils(Context context) {
        this.context = context;
        okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(CONNECT_OUT_TIME, TimeUnit.MILLISECONDS);
//        okHttpClient.setReadTimeout(READ_OUT_TIME, TimeUnit.MILLISECONDS);
//        okHttpClient.setWriteTimeout(WRITE_OUT_TIME, TimeUnit.MILLISECONDS);
        okHttpClient.setConnectionPool(ConnectionPool.getDefault());
    }

    /**
     * 获取OkhttpUtil操作类对象
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,9:20
     * <h3>UpdateTime</h3> 2016/4/7,9:20
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @return OkhttpUtil操作类对象
     */
    public static synchronized OKHttpUtils getHttpUtils(Context context) {
        if (httpUtils == null){
            httpUtils = new OKHttpUtils(context);
        }
        return httpUtils;
    }



    /**
     * 获取当前网络链接状态.
     *
     * @param context 上下文环境.
     * @return 有任意网络通畅时返回true, 否则返回false.
     * @version 1.0
     * @createTime 2013-3-5,下午4:21:27
     * @updateTime 2013-3-5,下午4:21:27
     * @createAuthor luzhenbang
     * @updateAuthor luzhenbang
     * @updateInfo
     */
    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE); // 获取网络服务
        if (connectivity == null) { // 判断网络服务是否为空
            return false;
        } else { // 判断当前是否有任意网络服务开启
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     *  get请求获取Request对象
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:05
     * <h3>UpdateTime</h3> 2016/4/6,21:05
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return Request对象
     */
    private  Request getRequest(String url){
        /*
         * 注意：1、通过Builder建造Request对象，默认是GET请求方式；
         *      2、使用post(RequestBody body)方法时，请求方式将会改变为POST
         *      3、也可以通过method(String method, RequestBody body)方法直接设置请求方式。
         */
        Request request = new Builder()
                .url(url)
                .build();
        return request;
    }

    /**
     *   post请求获取Request对象
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:42
     * <h3>UpdateTime</h3> 2016/4/6,21:42
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @param params 参数集合
     * @return Request对象
     */
    private Request getPostRequest(String url, HashMap<String, String> params){
        FormEncodingBuilder builder = new FormEncodingBuilder();
        Set<Map.Entry<String,String>> entries = params.entrySet();
        Iterator<Map.Entry<String,String>> iterator = entries.iterator();
        while (iterator.hasNext()){//添加参数   注意参数写入的格式都是UTF-8
            Map.Entry<String,String> entry=iterator.next();
            builder.add(entry.getKey(),entry.getValue());
        }
        RequestBody body = builder.build();//建立请求体
         /*
         * 注意：1、通过Builder建造Request对象，默认是GET请求方式；
         *      2、使用post(RequestBody body)方法时，请求方式将会改变为POST
         *      3、可以通过method(String method, RequestBody body)方法直接设置请求方式。
         */
        Request request = new Builder()
                .url(url)
                .post(body)
                .build();
        return request;
    }


    /**
     *   多文件post请求获取Request对象
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:42
     * <h3>UpdateTime</h3> 2016/4/6,21:42
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 服务器地址
     * @param params 参数集合
     * @param paths  文件路径集合
     * @return Request对象
     * @param key 后台接受文件的key值
     */
    private  Request getPostMultiRequest(String url,String key,HashMap<String, String> params,ArrayList<String> paths){
        MultipartBuilder builder = new MultipartBuilder().type(MultipartBuilder.FORM);
        Set<Map.Entry<String,String>> entries = params.entrySet();
        Iterator<Map.Entry<String,String>> iterator = entries.iterator();
        //添加参数 form-data; name="userId" 编码方式UTF-8
        while (iterator.hasNext()){
            Map.Entry<String,String> entry=iterator.next();
            builder.addFormDataPart(entry.getKey(), entry.getValue());
        }
        //添加文件 form-data; name="upload"; filename="b" 编码方式UTF-8
        for (String path : paths){
            File file = new File(path);
            //参数一：后台接受文件的key值，参数二：文件名字，参数三：RequestBody对象
            builder.addFormDataPart(key,file.getName(),RequestBody.create(MEDIA_TYPE_PNG,file));
        }
        RequestBody body = builder.build();
        Request request = new Builder()
                .url(url)
                .post(body)
                .build();
        return request;
    }

    /**
     * 获取多文件上传请求
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,11:40
     * <h3>UpdateTime</h3> 2016/4/7,11:40
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     * @param url 服务器地址
     * @param fileKeys 后台接受文件的key值数组
     * @param files 需要上传的文件数组
     * @param params 参数集合
     * @return Request对象
     */
    private  Request getPostMultiRequest(String url,String[] fileKeys,File[] files,HashMap<String, String> params){
        MultipartBuilder builder = new MultipartBuilder().type(MultipartBuilder.FORM);
        Set<Map.Entry<String,String>> entries = params.entrySet();
        Iterator<Map.Entry<String,String>> iterator = entries.iterator();
        //添加参数 form-data; name="userId"
        while (iterator.hasNext()){
            Map.Entry<String,String> entry=iterator.next();//Content-Disposition: form-data; name="ucode"
            builder.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + entry.getKey() + "\""),
                    RequestBody.create(null, entry.getValue()));
        }

        RequestBody fileBody = null;
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            String fileName = file.getName();
            fileBody = RequestBody.create(MediaType.parse(guessMimeType(fileName)), file);
            //TODO 根据文件名设置contentType
            // Content-Disposition: form-data; name="file"; filename="1463114525162.jpg"
            builder.addPart(Headers.of("Content-Disposition", "form-data; name=\"" + fileKeys[i] + "\"; filename=\"" + fileName + "\""), fileBody);
        }
        RequestBody requestBody = builder.build();
        return new Builder()
                .url(url)
                .post(requestBody)
                .build();
    }

    private String guessMimeType(String path) {
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String contentTypeFor = fileNameMap.getContentTypeFor(path);
        if (contentTypeFor == null) {
            contentTypeFor = "application/octet-stream";
        }
        return contentTypeFor;
    }


    /**
     *  获取Response响应对象
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:05
     * <h3>UpdateTime</h3> 2016/4/6,21:05
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return Response对象
     */
    public  Response getResponse(String url) throws IOException {
        Request request = getRequest(url);
        Response response = okHttpClient.newCall(request).execute();
        return response;
    }
    /**
     *  获取ResponseBody响应体对象
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:05
     * <h3>UpdateTime</h3> 2016/4/6,21:05
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return ResponseBody对象
     */
    public  ResponseBody getResponseBody (String url) throws IOException {
        Response response = getResponse(url);
        if (response.isSuccessful()){
            return response.body();
        }
        return null;
    }

    /**
     *  获取字符串
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:15
     * <h3>UpdateTime</h3> 2016/4/6,21:15
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return 字符串
     * @throws IOException
     */
    public String loadingString(String url) throws IOException {
        ResponseBody responseBody = getResponseBody(url);
        if (responseBody != null){
            return responseBody.string();
        }
        return  null;
    }
    /**
     *  获取字节数组
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:15
     * <h3>UpdateTime</h3> 2016/4/6,21:15
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return 返回字节数组
     * @throws IOException
     */
    public byte[] loadingBytes(String url) throws IOException {
        ResponseBody responseBody = getResponseBody(url);
        if (responseBody != null){
            return responseBody.bytes();
        }
        return  null;
    }
    /**
     *  获取字节流
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:15
     * <h3>UpdateTime</h3> 2016/4/6,21:15
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return 字节流
     * @throws IOException
     */
    public InputStream loadingInputStream(String url) throws IOException {
        ResponseBody responseBody = getResponseBody(url);
        if (responseBody != null){
            return responseBody.byteStream();
        }
        return  null;
    }

    /**
     *  获取Reader
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:15
     * <h3>UpdateTime</h3> 2016/4/6,21:15
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     * @return Reader
     * @throws IOException
     */
    public Reader loadingReader(String url) throws IOException {
        ResponseBody responseBody = getResponseBody(url);
        if (responseBody != null){
            return responseBody.charStream();
        }
        return  null;
    }


    /**
     * 发送get请求 网址
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:44
     * <h3>UpdateTime</h3> 2016/4/6,21:44
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     */
    public Call sendGetRequest(String url, final RequestCallInterface callInterf){
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            return null;
        }
        Request request = getRequest(url);
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils  sendGetRequest==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                return;
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()){
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils sendGetRequest==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            sendMessage(responseBean);
                            return;
                        }
                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils  sendGetRequest==========================>" + e.getMessage());
                    sendMessage(responseBean);
                    return;
                }
                sendMessage(responseBean);
            }
        });
        return call;
    }


    /**
     * 发送post异步请求 网址 （使用这个方法时，可以直接更新uI，但在同一时间之恩只能发起一次请求）
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:44
     * <h3>UpdateTime</h3> 2016/4/6,21:44
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     */
    public  Call sendPostRequest(String url, HashMap<String, String> params, final RequestCallInterface callInterf){
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)) {//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            return null;
        }
        Request request = getPostRequest(url, params);
        if (null == okHttpClient){
            getHttpUtils(context);
        }
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils sendPostRequest==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                return;
            }

            @Override
            public void onResponse(Response response) {
                try {
                    if (response.isSuccessful()){
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils sendPostRequest==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            sendMessage(responseBean);
                            return;
                        }
                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils  sendPostRequest==========================>" + e.getMessage());
                    sendMessage(responseBean);
                    return;
                }
                sendMessage(responseBean);
            }

        });

        return call;
    }


    /**
     * 发送post异步请求 网址
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:44
     * <h3>UpdateTime</h3> 2016/4/6,21:44
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 网址
     */
    public  Call sendPostRequest(String url, HashMap<String, String> params,Callback callback){
        Request request = getPostRequest(url, params);
        Call call = okHttpClient.newCall(request);
        call.enqueue(callback);
        return call;
    }


    /**
     * 发送post异步请求  (这个不能直接修改UI，同时间可以发出多个请求)
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/5/19,16:43
     * <h3>UpdateTime</h3> 2016/5/19,16:43
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     * @param url  网址
     * @param params 请求参数
     * @param callInterf 请求结果回调
     * @return 请求结果
     */
    public Call postAsyn(String url, HashMap<String, String> params,final RequestCallInterface callInterf){
        if (null != callInterf){//请求前需要处理的事
            callInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)) {//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            if (null != callInterf){//请求前需要处理的事
                callInterf.response(responseBean);
            }
            return null;
        }

        Request request = getPostRequest(url, params);
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils postAsyn==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                if (null != callInterf){//请求前需要处理的事
                    callInterf.response(responseBean);
                }
                return;
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    if (response.isSuccessful()){
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                            if (null != callInterf){
                                callInterf.response(responseBean);
                            }
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils postAsyn==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            if (null != callInterf){
                                callInterf.response(responseBean);
                            }
                            return;
                        }
                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        if (null != callInterf){
                            callInterf.response(responseBean);
                        }
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        if (null != callInterf){
                            callInterf.response(responseBean);
                        }
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        if (null != callInterf){
                            callInterf.response(responseBean);
                        }
                        return;
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        if (null != callInterf){
                            callInterf.response(responseBean);
                        }
                        return;
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils postAsyn==========================>" + e.getMessage());
                    if (null != callInterf){//请求前需要处理的事
                        callInterf.response(responseBean);
                    }
                    return;
                }
            }
        });
        return call;
    }


    /**
     *   上传单个文件
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:58
     * <h3>UpdateTime</h3> 2016/4/6,21:58
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url 服务器地址
     * @param filePath 文件路径
     */
    public Call uploadFile(String url,String filePath, final RequestCallInterface callInterf) {
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            //发送消息
            sendMessage(responseBean);
            return null;
        }
        //实例请求体
        RequestBody body = RequestBody.create(MEDIA_TYPE_MARKDOWN, new File(filePath));
        Request request = new Builder().url(url).post(body).build();
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils uploadFile==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                //发送消息
                sendMessage(responseBean);
                return;
            }
            @Override
            public void onResponse(Response response) {
                try {
                    if (response.isSuccessful()){
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils uploadFile==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            sendMessage(responseBean);
                            return;
                        }
                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils  uploadFile==========================>" + e.getMessage());
                    sendMessage(responseBean);
                    return;
                }
                sendMessage(responseBean);
            }
        });

        return call;
    }




    /**
     *   上传多文件
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:58
     * <h3>UpdateTime</h3> 2016/4/6,21:58
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url
     */
    public Call uploadFiles(String url,String[] fileKeys,File[] files,HashMap<String, String> params,final RequestCallInterface callInterf) {
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            return null;
        }
        Request request = getPostMultiRequest(url, fileKeys, files, params);
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils uploadFiles==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                return;
            }

            @Override
            public void onResponse(Response response) {
                try {
                    if (response.isSuccessful()) {
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils uploadFiles==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            sendMessage(responseBean);
                            return;
                        }
                    } else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR) {//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    } else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND) {//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    } else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    } else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }
                } catch (IOException e) {
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils uploadFiles==========================>" + e.getMessage());
                    sendMessage(responseBean);
                    return;
                }
                sendMessage(responseBean);
            }
        });

        return call;
    }


    /**
     *   上传多文件
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:58
     * <h3>UpdateTime</h3> 2016/4/6,21:58
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url
     */
    public Call upFiles(String url,String[] fileKeys,File[] files,HashMap<String, String> params,final RequestCallInterface callInterf) {
        requestCallInterf = callInterf;
        if (requestCallInterf == null){
            LogUtil.logErrorMessage("OKHttpUtils upFiles==========================> callInterf can not null !");
            return null;
        }
        requestCallInterf.before();
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
        }
        Request request = getPostMultiRequest(url, fileKeys, files, params);
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils upFiles==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                callInterf.response(responseBean);
                return;
            }

            @Override
            public void onResponse(Response response) {
                try {
                    if (response.isSuccessful()) {
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                            callInterf.response(responseBean);
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils upFiles==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            callInterf.response(responseBean);
                            return;
                        }
                    } else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR) {//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        callInterf.response(responseBean);
                        return;
                    } else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND) {//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        return;
                    } else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT) {//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        callInterf.response(responseBean);
                        return;
                    } else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        callInterf.response(responseBean);
                        return;
                    }
                } catch (IOException e) {
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils upFiles==========================>" + e.getMessage());
                    callInterf.response(responseBean);
                    return;
                }
            }
        });

        return call;
    }


    /**
     *   上传多文件
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:58
     * <h3>UpdateTime</h3> 2016/4/6,21:58
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param url
     */
    public Call uploadFiles(String url, String key, HashMap<String, String> params, ArrayList<String> paths,RequestCallInterface callInterf) {
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            return null;
        }
        Request request = getPostMultiRequest(url, key, params, paths);
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils uploadFiles==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                return;
            }
            @Override
            public void onResponse(Response response)  {
                try {
                    if (response.isSuccessful()){
                        String result = response.body().string();
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            responseBean.setStatus(jsonObject.optInt(OKHTTP_STATUS));
                            responseBean.setInfo(jsonObject.optString(OKHTTP_INFO));
                            responseBean.setData(jsonObject.optString(OKHTTP_DATA));
                        } catch (JSONException e) {
                            LogUtil.logErrorMessage("OKHttpUtils uploadFiles==========================>" + e.getMessage());
                            responseBean.setStatus(ResponseBean.STATUS_FAIL);//数据异常
                            responseBean.setInfo(context.getString(R.string.error_data_exception));
                            responseBean.setData(e.getMessage());
                            sendMessage(responseBean);
                            return;
                        }
                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                        sendMessage(responseBean);
                        return;
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils uploadFiles==========================>" + e.getMessage());
                    sendMessage(responseBean);
                    return;
                }
                sendMessage(responseBean);
            }
        });

        return call;
    }


    /**
     * 下载文件
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,16:46
     * <h3>UpdateTime</h3> 2016/4/7,16:46
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     * @param path 文件下载路径
     * @param savePath 文件的保存的路径
     * @param callInterf 结果回调
     * @return
     */
    public Call downLoadFile(String path, final String savePath,RequestCallInterface callInterf){
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            return null;
        }
        Request request = getRequest(path);
        call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils  downLoadFile==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                return;
            }

            @Override
            public void onResponse(Response response) throws IOException {
                File file;
                InputStream inputStream = null;
                OutputStream outputStream = null;
                try {
                    if (response.isSuccessful()){
                        file = new File(savePath);
                        file.createNewFile();
                        inputStream = response.body().byteStream();
                        outputStream = new FileOutputStream(file);
                        byte buffer[] = new byte[1024];
                        int read = 0;
                        while ((read = inputStream.read(buffer)) != -1) { // 读取信息循环写入文件
                            outputStream.write(buffer, 0, read);
                        }
                        outputStream.flush();

                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils downLoadFile==========================>" + e.getMessage());
                    return;
                }finally {//关闭流
                    if (inputStream != null){
                        inputStream.close();
                    }
                    if (outputStream != null){
                        outputStream.close();
                    }
                }
                sendMessage(responseBean);
            }
        });
        return call;
    }

    /**
     * 下载文件
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,16:46
     * <h3>UpdateTime</h3> 2016/4/7,16:46
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     * @param uiProgressResponseListener 下载进度监听入口
     * @param path 文件下载路径
     * @param savePath 文件的保存的路径
     * @param callInterf 结果回调
     * @return
     */
    public Call downLoadFile(UIProgressResponseListener uiProgressResponseListener,String path, final String savePath,RequestCallInterface callInterf){
        requestCallInterf = callInterf;
        if (requestCallInterf != null){
            requestCallInterf.before();
        }
        final ResponseBean responseBean = new ResponseBean();
        if (!isNetworkAvailable(this.context)){//网络异常
            responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
            responseBean.setInfo(context.getString(R.string.error_net_exception));
            responseBean.setData("");
            sendMessage(responseBean);
            return null;
        }
        Request request = getRequest(path);
        if (uiProgressResponseListener != null){
            call = ProgressHelper.addProgressResponseListener(okHttpClient,uiProgressResponseListener).newCall(request);
        }else {
            call = okHttpClient.newCall(request);
        }

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                LogUtil.logErrorMessage("OKHttpUtils  downLoadFile==========================>" + e.getMessage());
                responseBean.setStatus(ResponseBean.STATUS_FAIL);
                responseBean.setInfo(e.getMessage());
                responseBean.setData(request);
                sendMessage(responseBean);
                return;
            }

            @Override
            public void onResponse(Response response) throws IOException {
                File file;
                InputStream inputStream = null;
                OutputStream outputStream = null;
                try {
                    if (response.isSuccessful()){
                        file = new File(savePath);
                        file.createNewFile();
                        inputStream = response.body().byteStream();
                        outputStream = new FileOutputStream(file);
                        byte buffer[] = new byte[5*1024];
                        int read = 0;
                        while ((read = inputStream.read(buffer)) != -1) { // 读取信息循环写入文件
                            outputStream.write(buffer, 0, read);
                        }
                        outputStream.flush();
                        responseBean.setStatus(ResponseBean.STATUS_SUCCESS);
                        responseBean.setData(file);
                        responseBean.setInfo("下载完成");

                    }else if (response.code() == HttpURLConnection.HTTP_SERVER_ERROR){//服务器异常
                        responseBean.setStatus(ResponseBean.STATUS_SERVER_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_service_exception));
                        responseBean.setData(response.body().string());
                    }else if (response.code() == HttpURLConnection.HTTP_NOT_FOUND){//404
                        responseBean.setStatus(ResponseBean.STATUS_NET_ERROR);
                        responseBean.setInfo(context.getString(R.string.error_net_exception));
                        responseBean.setData(response.body().string());
                    }else if (response.code() == HttpURLConnection.HTTP_CLIENT_TIMEOUT){//链接超时
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(context.getString(R.string.error_connect_out_time));
                        responseBean.setData(response.body().string());
                    }else {
                        responseBean.setStatus(response.code());
                        responseBean.setInfo(response.message());
                        responseBean.setData(response.body().string());
                    }
                }catch (IOException e){
                    responseBean.setStatus(ResponseBean.STATUS_FAIL);
                    responseBean.setInfo(context.getString(R.string.error_data_exception));
                    responseBean.setData(e.getMessage());
                    LogUtil.logErrorMessage("OKHttpUtils downLoadFile==========================>" + e.getMessage());
                    return;
                }finally {//关闭流
                    if (inputStream != null){
                        inputStream.close();
                    }
                    if (outputStream != null){
                        outputStream.close();
                    }
                }
                sendMessage(responseBean);
            }
        });
        return call;
    }




    /**
     *
     * 发送信息
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,16:14
     * <h3>UpdateTime</h3> 2016/4/7,16:14
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     * @param responseBean
     */
    private void sendMessage(ResponseBean responseBean) {
        Message message = handler.obtainMessage();
        message.what = RESULT;
        message.obj = responseBean;
        handler.sendMessage(message);
    }


    /**
     *   关闭回调 (这个方法在Activity销毁时必须被调用) 节省内存  注意：不推荐使用该方法关闭
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/6,21:46
     * <h3>UpdateTime</h3> 2016/4/6,21:46
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     */
    public void stop(){
        if (call != null){
            call.cancel();
        }
    }



}
