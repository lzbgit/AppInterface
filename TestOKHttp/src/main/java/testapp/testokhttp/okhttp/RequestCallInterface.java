package testapp.testokhttp.okhttp;

import com.tentinet.digangchedriver.system.okhttp.ResponseBean;

/**
 * <h3>Description</h3> 请求回调接口
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2016/4/7 14:10
 * <h3>Copyright</h3> Copyright (c)2016 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public interface RequestCallInterface {

    /**
     * 网络请求之前
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,16:09
     * <h3>UpdateTime</h3> 2016/4/7,16:09
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     */
    void before();
    /**
     * 请求回调入口
     * <h3>Version</h3> 1.0
     * <h3>CreateTime</h3> 2016/4/7,14:15
     * <h3>UpdateTime</h3> 2016/4/7,14:15
     * <h3>CreateAuthor</h3> luzhenbang
     * <h3>UpdateAuthor</h3>
     * <h3>UpdateInfo</h3> (此处输入修改内容,若无修改可不写.)
     *
     * @param responseBean
     */
    void response(ResponseBean responseBean);

}
