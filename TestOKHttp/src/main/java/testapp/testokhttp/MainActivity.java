package testapp.testokhttp;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private OKHttpUtils okHttpUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        okHttpUtils = OKHttpUtils.getHttpUtils(this);
        HashMap<String ,String> stringHashMap = new HashMap<>();
        String url = "https://api.github.com/markdown/raw";
        stringHashMap.put("userId", "20115101106");
        ArrayList<String> strings = new ArrayList<>();
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/small.jpg");

        new UpFileBiz(this).uploadFile(url, file.getAbsolutePath(), new RequestCallInterface() {

            @Override
            public void before() {

            }

            @Override
            public void response(ResponseBean responseBean) {
                Toast.makeText(MainActivity.this,responseBean.getInfo(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (okHttpUtils != null){
            okHttpUtils.stop();
        }
    }
}
