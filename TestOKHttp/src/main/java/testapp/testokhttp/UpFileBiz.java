package testapp.testokhttp;

import android.content.Context;

/**
 * <h3>Description</h3>
 * TODO
 * <h3>Author</h3> luzhenbang
 * <h3>Date</h3> 2016/4/7 15:26
 * <h3>Copyright</h3> Copyright (c)2016 Shenzhen Tentinet Technology Co., Ltd. Inc. All rights reserved.
 */
public class UpFileBiz  {

    private final OKHttpUtils okHttpUtils;


    public UpFileBiz(Context context) {
        okHttpUtils = OKHttpUtils.getHttpUtils(context);
    }

    public void uploadFile(String url,String filePath , RequestCallInterface callInterf){
        okHttpUtils.uploadFile(url,filePath,callInterf);
    }
}
